# Hi, I'm Kavya

I'm a **Web developer** from <img src="https://www.flaticon.com/svg/static/icons/svg/3909/3909444.svg" width="17"/> India.

**📧 [Email](mailto:kavyamanoharan4217@gmail.com)** • **🐦 [Twitter](https://twitter.com/big1nt)** • **🔗 [LinkedIn](https://www.linkedin.com/in/yoginth)** • **🦊 [GitLab](https://gitlab.com/yo)**

### Academics

| Name | Course | CGPA | Year |
|------|--------|:----:|------|
| [**Dr. Mahalingam College of Engineering and Technology**](http://mcet.in) | B.Tech Information Technology | 8.6/10 | 2016 - 2020 |

### Work Experience

| Company | Designation | Year |
|---------|-------------|------|
| [**Kāladi Consulting, Chennai**](https://kaladi.in) |PHP developer - Web  | Jan 2020 - Present |

### Projects

| Name | Description | Techstack | Links |
|------|-------------|-----------|-------|
| [**Xpat**](https://apps.apple.com/in/app/xpat/id1507326139) | Expatriate Registration Mobile App during COVID-19 | <img alt="PHP" src="https://img.shields.io/badge/-PHP-8892BF?style=flat-square&logo=php&logoColor=white" /> <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-00758F?style=flat-square&logo=mysql&logoColor=white" /> <img alt="html5" src="https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white" /> <img alt="css" src="https://img.shields.io/badge/-CSS-264de4?style=flat-square&logo=css3&logoColor=white" /> <img alt="JavaScript" src="https://img.shields.io/badge/-JavaScript-F0DB4F?style=flat-square&logo=javascript&logoColor=white" /> <img alt="Postman" src="https://img.shields.io/badge/-Postman-EF5B25?style=flat-square&logo=postman&logoColor=white" /> <img alt="Talend API Tester" src="https://img.shields.io/badge/-Talend%20API%20Tester-6664d1?style=flat-square&logo=graphql&logoColor=white" /> <img alt="Bootstrap" src="https://img.shields.io/badge/-Bootstrap-7952b3?style=flat-square&logo=bootstrap&logoColor=white" /> | https://apps.apple.com/in/app/xpat/id1507326139 |
| **Kemployee** | Mobile app for employees to see latest news/blogs from the org | <img alt="PHP" src="https://img.shields.io/badge/-PHP-8892BF?style=flat-square&logo=php&logoColor=white" /> <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-00758F?style=flat-square&logo=mysql&logoColor=white" /> <img alt="html5" src="https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white" /> <img alt="css" src="https://img.shields.io/badge/-CSS-264de4?style=flat-square&logo=css3&logoColor=white" /> <img alt="JavaScript" src="https://img.shields.io/badge/-JavaScript-F0DB4F?style=flat-square&logo=javascript&logoColor=white" /> <img alt="Postman" src="https://img.shields.io/badge/-Postman-EF5B25?style=flat-square&logo=postman&logoColor=white" /> <img alt="Talend API Tester" src="https://img.shields.io/badge/-Talend%20API%20Tester-6664d1?style=flat-square&logo=graphql&logoColor=white" /> <img alt="Bootstrap" src="https://img.shields.io/badge/-Bootstrap-7952b3?style=flat-square&logo=bootstrap&logoColor=white" /> |  |
| **WWCVL Fleet** | Web app for expense booking for WWCVL employees | <img alt="PHP" src="https://img.shields.io/badge/-PHP-8892BF?style=flat-square&logo=php&logoColor=white" /> <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-00758F?style=flat-square&logo=mysql&logoColor=white" /> <img alt="Postman" src="https://img.shields.io/badge/-Postman-EF5B25?style=flat-square&logo=postman&logoColor=white" /> <img alt="Talend API Tester" src="https://img.shields.io/badge/-Talend%20API%20Tester-6664d1?style=flat-square&logo=graphql&logoColor=white" />  |  |
| **K-SFA** | Web app and API for Sales Force Automation | <img alt="PHP" src="https://img.shields.io/badge/-PHP-8892BF?style=flat-square&logo=php&logoColor=white" /> <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-00758F?style=flat-square&logo=mysql&logoColor=white" /> <img alt="html5" src="https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white" /> <img alt="css" src="https://img.shields.io/badge/-CSS-264de4?style=flat-square&logo=css3&logoColor=white" /> <img alt="JavaScript" src="https://img.shields.io/badge/-JavaScript-F0DB4F?style=flat-square&logo=javascript&logoColor=white" /> <img alt="Postman" src="https://img.shields.io/badge/-Postman-EF5B25?style=flat-square&logo=postman&logoColor=white" /> <img alt="Talend API Tester" src="https://img.shields.io/badge/-Talend%20API%20Tester-6664d1?style=flat-square&logo=graphql&logoColor=white" /> <img alt="Bootstrap" src="https://img.shields.io/badge/-Bootstrap-7952b3?style=flat-square&logo=bootstrap&logoColor=white" /> |
| **K-Market** | e-commerce app | <img alt="PHP" src="https://img.shields.io/badge/-PHP-8892BF?style=flat-square&logo=php&logoColor=white" /> <img alt="MySQL" src="https://img.shields.io/badge/-MySQL-00758F?style=flat-square&logo=mysql&logoColor=white" /> <img alt="Postman" src="https://img.shields.io/badge/-Postman-EF5B25?style=flat-square&logo=postman&logoColor=white" /> <img alt="Talend API Tester" src="https://img.shields.io/badge/-Talend%20API%20Tester-6664d1?style=flat-square&logo=graphql&logoColor=white" /> |

### Skills

MySQL, PHP, HTML, CSS, JavaScript
